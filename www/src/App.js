import React, { Component } from 'react';
import ContactForm from './Contact/ContactForm';
import ContactTable from './Contact/ContactTable';
import ReactLoad from './Utils/ReactLoad';
import axios from 'axios';
import MyModal from './Utils/Modal';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { checkSession, config,logOut, authenticateUser } from './Utils/Config'
import { Amplify, Auth} from "aws-amplify";
import {AmplifyAuthenticator, AmplifySignOut, AmplifySignIn} from "@aws-amplify/ui-react";
import { AuthState, onAuthUIStateChange } from '@aws-amplify/ui-components';
import { Navbar, Container, NavDropdown } from 'react-bootstrap'
import awsconfig from "./aws-exports";
import './App.css';
import UploadFile from './Contact/UploadFile';
import { faSync, faSearch,faAngleDoubleRight } from '@fortawesome/free-solid-svg-icons'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      contacts: [],
      totalContacts: 0,
      pageCount: 0,
      last_eval_key: '',
      file_search: '',
      isLoading: true,
      modal_isOpen: false,
      modal_title: '',
      modal_message: '',
      modal_showIcon: 0,
      tokenId: '',
      usern: ''
    };
    this.updateContactList = this.updateContactList.bind(this);
    this.showLoading = this.showLoading.bind(this);
    this.hideLoading = this.hideLoading.bind(this);
    this.logoutHandle = this.logoutHandle.bind(this);
    this.loginHandle = this.loginHandle.bind(this);
    this.handleSearchChange = this.handleSearchChange.bind(this);
    this.searchFile = this.searchFile.bind(this);
    this.refresh = this.refresh.bind(this);
    //this.checkUser = this.checkUser.bind(this);
    Amplify.configure(awsconfig);
  }

  logoutHandle(){
    logOut();
  }

  loginHandle(){
    let tok = authenticateUser('asd@example.com','asd123456');
    if( tok === 'Login error'){
      this.showModal('User',tok,2);
    }else{
      this.showModal('User','Sign in successful',1);
    }
  }

  componentDidMount() {
    //this.updateContactList();
    onAuthUIStateChange((nextAuthState, authData) => {
      this.updateContactList();
    });
  }

  async updateContactList(file=''){
    let tokenId = await checkSession();
    if( tokenId === 'User not found.'){
      this.hideLoading('');
      this.showModal('Error auth',tokenId, 2);
      return;
    }
    
    this.showLoading('Loading contacts');
    //process.env.REACT_APP_API_ENDPOINT_GET_ALL
    let url = config.url;
    this.setState({
      contacts:[]
    });

    axios.get(url,{
      headers:{
        'Authorization': 'Bearer ' + tokenId
      },
      params: {
        'last_eval_key': this.state.last_eval_key,
        'filename': this.state.file_search,
        'limit': config.itemsPerPage
      }
    })
    .then(response => {
     
      this.setState({ 
        contacts: response.data.contact_items,
        totalContacts: response.data.total_count,
        pageCount: response.data.page_count,
        last_eval_key: response.data.last_eval_key,
        isLoading: false,
        message: ''
      });
    })
    .catch(error => {
        console.log(error);
        this.hideLoading('');
    });
  }

  showLoading(message = "Loading"){
    this.setState({ isLoading: true, message: message  });
  }

  hideLoading(message = ""){
    this.setState({ isLoading: false, message: message  });
  }

  showModal = (title, message, _showIcon = 0) =>{
    this.setState({
      modal_isOpen: true,
      modal_title: title,
      modal_message: message,
      modal_showIcon: _showIcon
    });
  }

  closeModal = () =>{
    this.setState({
      modal_isOpen: false
    });
  }

  handleSearchChange(event){
    this.setState({ [event.target.name]: event.target.value.trim() });
  }

  searchFile(){
    this.setState({
        last_eval_key: '',
    }, this.updateContactList);
  }

  refresh(){
    this.setState({
        last_eval_key: '',
    }, this.updateContactList);
  }

  render() {
    const { contacts,totalContacts,pageCount,isLoading,message, modal_isOpen, modal_message, modal_title, modal_showIcon } = this.state;
    
    function UserName(){
      if( Auth.user !== null){
        if( Auth.user.attributes.email !==null )
          return Auth.user.attributes.email;
      }
      return '';
    }

    function DisabledPage(key){
      console.log(key);
      if( key !== undefined){
        return 'page-item';
      }else{
        return 'page-item disabled';
      }
      
    }
    
    return (
      <AmplifyAuthenticator usernameAlias="email">
    
        <AmplifySignIn
          headerText="Phone Book Sign in"
          slot="sign-in"
          usernameAlias="email"
        ></AmplifySignIn>
        <header>
          <Navbar className="bg-secondary text-light">
          <Container>
            <Navbar.Brand href="#home" className="text-light"><h1>Phone Book</h1></Navbar.Brand>
            <Navbar.Toggle />
            <Navbar.Collapse className="justify-content-end text-light">
              <NavDropdown title={ UserName() } id="collasible-nav-dropdown">
                <NavDropdown.Item href="#" onClick={ this.logoutHandle }>Sign out</NavDropdown.Item>
              </NavDropdown>
            </Navbar.Collapse>
          </Container>
        </Navbar>
        </header>

        <section>
          <div className="container mt-4">
          
            <MyModal isOpen={modal_isOpen} title={modal_title} message={modal_message} close={this.closeModal} showIcon={modal_showIcon}></MyModal>
            
            <ContactForm 
                updateTable={ this.updateContactList } 
                showLoading={ this.showLoading } 
                hideLoading={ this.hideLoading }
                showModal={ this.showModal }
                closeModal={ this.closeModal }
            ></ContactForm>
            <UploadFile
              updateTable={ this.updateContactList } 
              showLoading={ this.showLoading } 
              hideLoading={ this.hideLoading }
              showModal={ this.showModal }
              closeModal={ this.closeModal }
            />
            <div className="row mt-2 mb-2">
              <div className="col-8">
                <ReactLoad isLoading={ isLoading } message={ message }></ReactLoad>
              </div>
            {/*</div>
            <div className="row mt-2 mb-2">
              
              <div className="col-4">
                <h4>Total items in search: { totalContacts }</h4>
              </div>

              <div className="col-4">
                <h4>Items per page: { pageCount }</h4>
              </div>*/
              }
              { 
              <div className="col-4">
                <ul className="pagination">
                  <li className="page-item">
                    <a className="page-link" href="#" onClick={ this.refresh }>
                      <FontAwesomeIcon icon={faSync} />
                    </a>
                  </li>
                  <li className={ DisabledPage(this.state.last_eval_key) }>
                    <a className="page-link" href="#" aria-label="Next" onClick={ ()=> this.updateContactList() }>
                    <FontAwesomeIcon icon={faAngleDoubleRight} />
                    </a>
                  </li>
                  <li className="page-item">
                    <form className="form-inline">
                      <div className="input-group">
                        <input type="text" className="form-control" placeholder="File name..." aria-label="File name" aria-describedby="basic-addon1" 
                        onChange={this.handleSearchChange} 
                        defaultValue={ this.state.file_search || ''}
                        name="file_search"
                        />
                        <div className="input-group-append">
                          <button className="btn btn-outline-secondary" type="button"
                          onClick={ this.searchFile }
                          >
                            <FontAwesomeIcon icon={faSearch} />
                          </button>
                        </div>
                      </div>
                    </form>
                  </li>
                </ul>
              </div>
              }
            </div>
            <ContactTable 
                contacts={contacts} 
                totalContacts={totalContacts}
                updateTable={ this.updateContactList } 
                showLoading={ this.showLoading } 
                hideLoading={ this.hideLoading }
                showModal={ this.showModal }
                closeModal={ this.closeModal }
            />
          </div>
        </section>
      </AmplifyAuthenticator>
    );
  }
}


export default App;
