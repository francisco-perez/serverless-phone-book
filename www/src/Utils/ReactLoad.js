import { Component } from 'react';

class ReactLoad extends Component {
    
    render() {
      
      return (
        <div className={`d-flex align-items-center ${this.props.isLoading ? "" : "d-none"}`}>
            <strong>{ this.props.message }...</strong>
            <div className="spinner-border ml-auto" role="status" aria-hidden="true"></div>
        </div>
      );
    }
}
  
  
export default ReactLoad;