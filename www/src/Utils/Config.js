import * as  AmazonCognitoIdentity from "amazon-cognito-identity-js";
import { Auth } from "aws-amplify";
import { AuthState } from '@aws-amplify/ui-components';
export const config = {
    //url: 'https://hhtqul046l.execute-api.us-east-1.amazonaws.com/Prod/'
    url: 'http://127.0.0.1:3000/',
    itemsPerPage: 10,
}

export async function logOut(){
    
    /*const poolData = {UserPoolId: "us-east-1_NBIAWoUBu", ClientId: "7baa4e575bb51pv8u0adjdtpnh"};
    const userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);
    const cognitoUser = userPool.getCurrentUser();
    if (cognitoUser != null) {
        console.log('Logged out');
        cognitoUser.signOut();
        
    }else{
        console.log('No cognito user to sign out');
    }*/
    try {
        await Auth.signOut();
    } catch (error) {
        console.log('error signing out: ', error);
    }
}

export async function checkSession(user, pwd){
    return new Promise((resolve, reject) => {
        
        /*const poolData = {UserPoolId: "us-east-1_NBIAWoUBu", ClientId: "7baa4e575bb51pv8u0adjdtpnh"};
        const userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);
    
        const cognitoUser = userPool.getCurrentUser();
        let tok = '';
        if (cognitoUser != null) {
          cognitoUser.getSession((err, session) => {
            if (err) {
              console.log("ERROR FROM GET SESSION 1: ");
              console.log(err);
              tok = err;
            } else if (!session.isValid()) {
              console.log("ERROR FROM GET SESSION 2: ");
              //tok = this.authenticateUser(user,pwd);
            } else {
              tok = session.getIdToken().getJwtToken();
              //console.log(session.getAccessToken().getJwtToken());
            }
            resolve(tok);
          });
        } else {
            tok = 'User not found.';
            console.log(tok);
            resolve(tok);
        }*/
        if( AuthState.SignIn ){
            Auth.currentSession().then(data => {
                resolve(data.idToken.jwtToken)
            });
        }else{
            let tok = 'User not found.';
            resolve(tok);

        }
        
    });
};

export const authenticateUser = function(user, pwd){
    return new Promise((resolve, reject) => {
        const poolData = {UserPoolId: "us-east-1_NBIAWoUBu", ClientId: "7baa4e575bb51pv8u0adjdtpnh"};
        const userPool = new AmazonCognitoIdentity.CognitoUserPool(poolData);
        //console.log("USER: " + user + " PWD: " + pwd);
        let email = user;
        let password = pwd;
        let authenticationData = {
            Username: email,
            Password: password,
        };

        let authenticationDetails = new AmazonCognitoIdentity.AuthenticationDetails(authenticationData);

        let userData = {
            Username: email,
            Pool: userPool
        };

        let cognitoUser = new AmazonCognitoIdentity.CognitoUser(userData);

        cognitoUser.authenticateUser(authenticationDetails, {
                onSuccess: function (result) {
                    
                    let idToken = result.idToken.jwtToken;
                    console.log("Token success: " + idToken);
                    resolve(idToken);
                },
                onFailure: function (err) {

                    console.log("Err on failure");
                    console.log(err);
                    resolve("Login error");
                }
            }
        );
    });
}

export default config;