import { Component, createRef } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import Form from 'react-bootstrap/Form'
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/button'

class ModalForm extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            first_name: this.props.contact.first_name,
            last_name: this.props.contact.last_name,
            phone: this.props.contact.phone,
            id: this.props.contact.id
        }
        this.textFirstName = createRef();
        this.textLastName = createRef();
        this.textPhone = createRef();
        this.nodeRef = createRef(null);
    }
  
    render(){
        //const { first_name, last_name, phone } = this.state;
        return(
            
            <>
                <Modal 
                    show={this.props.isOpen} 
                    onHide={this.props.closeModal}
                    //onShow={ this.show }
                    ref={this.nodeRef}
                >
                <Modal.Header>
                    <Modal.Title>Edit contact</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form.Group >
                        <Form.Label>First Name: </Form.Label>
                        <Form.Control type="text" placeholder="First name..."
                            onChange={this.handleChange}
                            defaultValue={ this.props.contact.first_name }
                            ref={ this.textFirstName }
                        />
                    </Form.Group>

                    <Form.Group >
                        <Form.Label>Last Name: </Form.Label>
                        <Form.Control type="text" placeholder="Last name..."
                            onChange={this.handleChange} 
                            defaultValue={ this.props.contact.last_name }
                            ref={ this.textLastName }
                        />
                    </Form.Group>

                    <Form.Group >
                        <Form.Label>Phone: </Form.Label>
                        <Form.Control type="text" placeholder="Phone..."
                            onChange={this.handleChange} 
                            defaultValue={ this.props.contact.phone }
                            ref={ this.textPhone }
                        />

                    </Form.Group>
                </Modal.Body>

                <Modal.Footer>
                    <Button variant="secondary" onClick={ this.props.closeModal }>Cancel</Button>
                    <Button variant="primary" type="submit" 
                        onClick={() => this.props.updateContact(
                            this.textFirstName.current.value,
                            this.textLastName.current.value,
                            this.textPhone.current.value,
                            this.props.contact.id
                        )}>
                        Save
                    </Button>
                </Modal.Footer>
                </Modal>
            </>
        )
    }
}

export default ModalForm;