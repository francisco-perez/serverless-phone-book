import { Component, createRef } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import Modal from 'react-bootstrap/Modal'
import Button from 'react-bootstrap/button'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

class MyModal extends Component {
    
    constructor(props) {
        super(props);
        this.nodeRef = createRef(null);
    }

    render(){
        const { isOpen, title, message } = this.props;
        const closeModal = this.props.close;

        function IconOK(props){
            return <i className="text-success ml-2">&nbsp;&nbsp;<FontAwesomeIcon icon="check-square" size="2x"/></i>;
        }

        function IconError(props){
            return <i className="text-danger ml-2">&nbsp;&nbsp;<FontAwesomeIcon icon="times-circle" size="2x"/></i>;
        }

        function Icon(props){
            const showIcon = props.show;
            if( showIcon === 1){
                return <IconOK/>;
            }else if( showIcon === 2){
                return <IconError/>;
            }else{
                return <i/>
            }
        }

        return(
            
            <>
                <Modal 
                    show={ isOpen } 
                    onHide={ closeModal }
                    ref={this.nodeRef}
                    animation={false}
                >
                <Modal.Header>
                    <Modal.Title>{ title }</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    { message }
                    <Icon show={ this.props.showIcon} />
                </Modal.Body>

                <Modal.Footer>
                    <Button variant="secondary" onClick={ closeModal }>Close</Button>
                </Modal.Footer>
                </Modal>
            </>
        )
    }
}

export default MyModal;