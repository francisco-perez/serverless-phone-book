import React, { Component } from 'react';
import axios from 'axios';
import { checkSession, config } from '../Utils/Config'
class ContactTableRow extends Component {

    constructor (props) {
        super(props)
        this.state = {
            contact: props.contact,
            number: props.number + 1
        }
        this.deleteContact = this.deleteContact.bind(this);

    }
  
    async deleteContact(id){
        let tokenId = await checkSession();
        console.log("TOKEN ID DELETE:" + tokenId);
        if( tokenId === 'User not found.'){
            this.props.showModal('User','User not found. Must login',2);
            return;
        }        
        
        this.props.showLoading('Deleting contact ' + id);
        //process.env.REACT_APP_API_ENDPOINT_DELETE + id
        let url = config.url + id; 
        axios.delete(url,{
            'headers':{
                'Authorization': 'Bearer ' + tokenId
            }
        })
        .then(response => {
            let data = response.data;
            if( data.success !== null && data.success !== undefined
                && data.success){
                    this.props.updateTable();
                    this.props.showModal('Contact deleted', data.message,1);
            }else{
                this.props.showModal('Contact',data.message,2);
            }
            
        })
        .catch(error=>{
            console.log(error);
            this.props.hideLoading('');
            this.hideParentLoading('');
            let errMsg = 'Error from server creating contact.';
            if( error.response!==null && error.response.data !== undefined && error.response.data !== null)
                errMsg = error.response.data.message;
            this.props.showModal('Contact',errMsg,2);
        })
    }

    render() {
        const { contact,number } = this.state
        return (
            <tr>
                <td>
                    { number }    
                </td>
                <td>
                    { contact.id }
                </td>
                <td>
                    { contact.first_name }
                </td>
                <td>
                    { contact.last_name }
                </td>
                <td>
                    { contact.phone }
                </td>
                <td>
                    { contact.file }
                </td>
                <td>
                <div className="text-center">
                    <button id='add' className='btn btn-block btn-primary mr-2' onClick={ ()=>this.props.showModalForm(contact) }>Edit</button>&nbsp;&nbsp;
                    <button id='add' className='btn btn-block btn-danger ml-2' onClick={ () => this.deleteContact(contact.id) }>Delete</button>
                </div>
                </td>
            </tr>
        );
    }
}

export default ContactTableRow;