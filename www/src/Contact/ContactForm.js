import React, { Component } from 'react';
import axios from 'axios';
import { checkSession, config } from '../Utils/Config'
class ContactForm extends Component {
    constructor (props) {
        super(props)
        this.state = {
            first_name: '',
            last_name: '',
            phone: '',
        }
        this._handleChange = this._handleChange.bind(this);
        this.createContact = this.createContact.bind(this);
        //this.createCall = this.createCall.bind(this);
        this.showParentLoading = props.showLoading;
        this.hideParentLoading = props.hideLoading;
        this.refreshContactList = props.updateTable;
    }
    
    showParentLoading(message){}
    hideParentLoading(message){}
    refreshContactList(){}

    _handleChange(event){
        this.setState({ [event.target.name]: event.target.value });
    }

    async createContact(){
        const { first_name, last_name, phone } = this.state;
        if( first_name.length==0 || last_name.length ==0 || phone.length == 0){
            this.props.showModal('Contact error','First name, last name and phone are required.',2);
            return;
        }

        let tokenId = await checkSession();
        console.log("TOKEN ID CREATE:" + tokenId);
        if( tokenId === 'User not found.'){
            this.props.showModal('Contact','User not found. Must login',2);
            return;
        }        
        
        try{    
            this.showParentLoading('Creating new contact');
            let payload = {
                "first_name": first_name,
                "last_name": last_name,
                "phone": phone
            };

            //process.env.REACT_APP_API_ENDPOINT_CREATE
            let url = config.url;
            axios.post( url , payload,{
                'headers':{
                  'Authorization': 'Bearer ' + tokenId
                }
              })
            .then( res => {
                let data= res.data;
                if( data.success !== null && data.success !== undefined
                    && data.success){
                    this.refreshContactList();
                    //alert(data.message);
                    this.props.showModal('Contact created',data.message,1);
                }else{
                    this.props.showModal('Contact',data.message,2);
                }
            })
            .catch(error => {
                console.log(error.response.data);
                this.hideParentLoading('');
                let errMsg = 'Error from server creating contact.';
                if( error.response!==null && error.response.data !== undefined && error.response.data !== null)
                    errMsg = error.response.data.message;
                this.props.showModal('Contact',errMsg,2);
            });
        }catch(error){
            console.log(error);
            this.hideParentLoading('');
            this.props.showModal('Contact','Error creating contact.',2);
        }
        
        return false;
    }

    /*createCall(body_data){
        return fetch('http://127.0.0.1:3000/',
            {
                method: "POST",
                body: body_data
            })
            .then(res => res.json())
            .then((data) => {
                this.setState({ 
                    contacts: data,
                    totalContacts: data.length,
                    isLoading: false
                })
                console.log(this.state.contacts)
            })
            .catch(function(err){
                console.log(err);
            })
    }*/

    render() {
        const { first_name,last_name,  phone } = this.state
        return (
            <form className="mb-4">
                <div className="row">
                    <div className="form-group col-md-3">
                        <label htmlFor="first_name">First Name</label>
                        <input type="text" className="form-control" name="first_name" id="first_name" placeholder="First Name..." 
                            onChange={this._handleChange} 
                            defaultValue={ first_name || ''} 
                            required/>
                    </div>

                    <div className="form-group col-md-3">
                        <label htmlFor="last_name">Last Name</label>
                        <input type="text" className="form-control" name="last_name" id="last_name" placeholder="Last name..." 
                            onChange={this._handleChange} 
                            defaultValue={ last_name || ''}
                            required/>
                    </div>

                    <div className="form-group col-md-3">
                        <label htmlFor="phone">Phone</label>
                        <input type="text" className="form-control" name="phone" id="phone" placeholder="Phone..." 
                            onChange={this._handleChange} 
                            defaultValue={ phone || ''}
                            required/>
                    </div>

                    <div className="col">
                        <button id='add' className='btn btn-block btn-success pl-4 pr-4 mt-4' 
                                type="button"
                                onClick={this.createContact}
                        >Create contact</button>
                    </div>
                </div>
            </form>
        );
    }
}


export default ContactForm;