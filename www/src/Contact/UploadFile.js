import React, { Component } from 'react';
import axios from 'axios';
import { checkSession, config } from '../Utils/Config'
import { formatUrl } from "@aws-sdk/util-format-url"
class UploadFile extends Component {
    constructor (props) {
        super(props)
        this.state = {
            file: null,
        }
        this.getPresignedURL = this.getPresignedURL.bind(this);
        this.showParentLoading = props.showLoading;
        this.hideParentLoading = props.hideLoading;
        this.refreshContactList = props.updateTable;
        this.submitFile = this.submitFile.bind(this);
        this.handleFileUpload = this.handleFileUpload.bind(this);
    }
    
    showParentLoading(message){}
    hideParentLoading(message){}
    refreshContactList(){}

    async getPresignedURL(tokenId, fileName){
        
        
        const url = config.url + 'fileurl';
        const s3 = new Promise((resolve,reject)=>{
            axios.get( url ,{
            'headers':{
                'Authorization': 'Bearer ' + tokenId
            }
            }).then( data =>{ resolve(data) });
        });
        
        if( s3!== null && s3.data !== null)
            return s3;
        else
            throw 'Error';
        
    }

    async submitFile(event) {
        event.preventDefault();
        let tokenId = await checkSession();
        console.log("TOKEN ID FILEURL:" + tokenId);
        if( tokenId === 'User not found.'){
            this.props.showModal('Contact','User not found. Must login',2);
            return;
        }        
        
        this.showParentLoading('Getting presigned URL');
        if( this.state.file===null ){
            this.hideParentLoading('');
            this.props.showModal('File','Please select a csv file',2);
        }else{
            const s3URL = await this.getPresignedURL(tokenId, this.state.file[0].name)
            this.showParentLoading('Uploading file to S3');
            // ====== GET PRESIGNED URL =========
            axios.put( s3URL.data.uploadURL,this.state.file[0],{
                headers: {
                    'Content-Type': this.state.file[0].type,
                }
            })
            .then((response) => {

                // ====== PROCESS FILE TO DYNAMO =========
                this.showParentLoading('Processing file to Dynamo');
                const url = config.url + 'file';
                axios.put( url,{
                    'key': s3URL.data.Key
                },{
                    'headers':{
                        'Authorization': 'Bearer ' + tokenId
                        
                    }
                }).then((response) => {
                    this.props.updateTable();
                    this.props.showModal('File','File uploaded successfully',1);
                    this.hideParentLoading('');
                    
                    console.log(response);
                }).catch((err)=>{
                    this.hideParentLoading('Error');
                    this.props.showModal('File','Error',2);
                    console.log("Error saving in db s3");
                    console.log(err);
                })
            })
            .catch((error)=>{
                this.hideParentLoading('Error');
                console.log("Error uploading file to s3");
                console.log(error);
            })
        }
    };

    handleFileUpload = (event) => {
        this.setState({ file: event.target.files });
    };

    render() {
        
        return (
            <form onSubmit={this.submitFile}>
                <input
                    label="Upload file"
                    type="file"
                    onChange={this.handleFileUpload}
                />
                <button type="submit" className="btn btn-primary">Upload a csv</button>
            </form>
        );
    }
}


export default UploadFile;