import React, { Component } from 'react';
import ModalForm from '../Utils/ModalForm';
import ContactTableRow from './ContactTableRow';
import axios from 'axios';
import { checkSession, config } from '../Utils/Config'
class ContactTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            show: false,
            modalContact: {}
        };
        this.updateContact = this.updateContact.bind(this);
    }

    showModalForm = (contact) =>{
        this.setState({ show: true, modalContact: contact });
    }

    closeModal = () =>{
        this.setState({ show: false });
    }

    async updateContact(name, last_name, phone, id){
        let tokenId = await checkSession();
        console.log("TOKEN ID UPDATE:" + tokenId);
        if( tokenId === 'User not found.'){
            this.props.showModal('Contact','User not found. Must login',2);
            return;
        }

        try{    
            this.closeModal();
            this.props.showLoading('Updating contact ' + id);
           
            let payload = {
                "first_name": name,
                "last_name": last_name,
                "phone": phone
            };
            //console.log(payload);
            //process.env.REACT_APP_API_ENDPOINT_UPDATE + id
            let url = config.url + id
            axios.put( url, payload,{
                'headers':{
                  'Authorization': 'Bearer ' + tokenId
                }
            })
            .then( res => {
                
                console.log(res);
                let data= res.data;
                if( data.success !== null && data.success !== undefined
                    && data.success){
                    this.props.updateTable();
                    this.props.showModal('Contact',data.message,1);
                    
                }else{
                    this.props.showModal('Contact',data.message,2);
                }
            })
            .catch(error => {
                console.log(error);
                this.props.hideLoading('');
                let errMsg = 'Error from server updating contact.';
                if( error.response!==null && error.response.data !== undefined && error.response.data !== null)
                    errMsg = error.response.data.message;
                this.props.showModal('Contact',errMsg,2);
            });
        }catch(error){
            this.props.hideLoading('');
            this.props.showModal('Contact','Error updating contact.',2);
        }
    }

    render() {
        const { show , modalContact } = this.state;
        return (
        <>
            <ModalForm 
                closeModal={this.closeModal} 
                isOpen={show} 
                contact={ modalContact }
                updateContact={this.updateContact}
                updateTable={ this.props.updateTable }
            ></ModalForm>
            <table id='table1' className="table table-bordered table-hover table-striped table-responsive">
                <thead className="table-dark">
                <tr className="text-center">
                    <th>No</th>
                    <th>Id</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Phone</th>
                    <th>File</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                {   
                    this.props.contacts.map( (contact,index) => (
                        <ContactTableRow 
                            contact={contact} 
                            number={index} 
                            key={index}
                            updateTable={ this.props.updateTable }
                            showLoading={ this.props.showLoading }
                            hideLoading={ this.props.hideLoading }
                            showModalForm={ this.showModalForm }
                            showModal={ this.props.showModal}
                        />
                    ))
                }
                </tbody>
                <tfoot>  
                </tfoot>
            </table>
        </>
        );
    }
}


export default ContactTable;
