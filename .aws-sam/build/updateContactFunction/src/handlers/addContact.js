const { v4: uuidv4 } = require('uuid');
/**
 * Add a contact to phone book table
 */
exports.addContactHandler = async (event) => {
    if ( event.httpMethod !== null && event.httpMethod !== undefined 
        && event.httpMethod !== 'POST') {
        throw new Error(`This function only accepts POST method, you tried: ${event.httpMethod} method.`);
    }
    
    const dataBase = require('../database/DBManager');
    dataBase.vars.databaseType = process.env.DB_TYPE;
    const contact = require('../contact/create-update');
    let id = uuidv4();
    const response = contact.createUpdate(event, dataBase, id);
    return response;
}
