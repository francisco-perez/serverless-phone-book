exports.getClient = () => {
    const dynamodb = require('aws-sdk/clients/dynamodb');
    const paramsEnv = process.env;
    const runEnv = paramsEnv.RUN_ENV;

    let docClient = new dynamodb.DocumentClient();

    if( runEnv !== null && runEnv === "LOCAL")
    {
        let endpoint = paramsEnv.LOCAL_DB_HOST + ':' + paramsEnv.LOCAL_DB_PORT;
        docClient = new dynamodb.DocumentClient({
            region: paramsEnv.LOCAL_DB_REGION,
            endpoint: endpoint,
            accessKeyId: paramsEnv.LOCAL_DB_ACCESS_KEY,
            secretAccessKey: paramsEnv.LOCAL_DB_SECRET_ACCESS_KEY
        });   
        //console.info("RUNNING LOCAL: " + endpoint);
    }
    return docClient;
}