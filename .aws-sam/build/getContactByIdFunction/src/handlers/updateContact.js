/**
 * Add a contact to phone book table
 */
exports.updateContactHandler = async (event) => {
    if ( event.httpMethod !== null && event.httpMethod !== undefined 
        && event.httpMethod !== 'PUT') {
        throw new Error(`This function only accepts PUT method, you tried: ${event.httpMethod} method.`);
    }
    
    const dataBase = require('../database/DBManager');
    dataBase.vars.databaseType = process.env.DB_TYPE;
    const contact = require('../contact/create-update');
    const response = contact.createUpdate(event, dataBase);
    return response;
}
