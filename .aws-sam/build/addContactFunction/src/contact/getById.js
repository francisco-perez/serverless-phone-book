module.exports.getById = async (event, dataBase) =>{
    let statusCode = 200;
    let contact = '';
    try{
        const tableName = dataBase.vars.tableName;
        const docClient = dataBase.DB();
        
        if( event.pathParameters !== null && event.pathParameters !== undefined
            && event.pathParameters.id !== null && event.pathParameters.id !== undefined)
        {
            const v_id = event.pathParameters.id;
            var params = {
                TableName : tableName,
                Key: { id: v_id },
            };
            
            const data = await docClient.get(params).promise();
            if( data.Item === null || data.Item === undefined){
                statusCode = 404;
            }else{
                contact = data.Item;
            }
        }

    }catch(error){
        statusCode = 500;
        console.info(JSON.stringify(error));
        if(error !== undefined && error.message !== undefined){
            if( error.message == 'The conditional request failed' 
                ||  (error.code!==null && error.code!==undefined && error.code == 'ConditionalCheckFailedException'))
            {
                statusCode = 404;
            }
        }
    }

    const response = {
        statusCode: statusCode,
        headers: {
            'Access-Control-Allow-Origin' : '*'
        },
    };

    if( statusCode == 200){
        response.body =  JSON.stringify(contact);
    }
    return response;
}