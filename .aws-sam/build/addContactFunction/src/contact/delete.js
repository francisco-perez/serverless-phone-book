module.exports.delete = async (event, dataBase) => {

    let success = false;
    let msg = 'Error trying to delete contact.';
    let statusCode = 404;
    let v_id = '';
    try{
        const tableName = dataBase.vars.tableName;
        const docClient = dataBase.DB();
        
        if( event.pathParameters !== null && event.pathParameters !== undefined
            && event.pathParameters.id !== null && event.pathParameters.id !== undefined)
        {
            v_id = event.pathParameters.id;
            let params = {
                TableName: tableName,
                Key: {
                    id: v_id
                }
            };
            
            const result = await docClient.delete( params ).promise();
            statusCode = 200;
            success = true;
            msg = 'Contact deleted successfully!';
            console.info('DB passed');
        }
    }catch(error){
        statusCode = 500;
        console.log(JSON.stringify(error));
    }

    const response = {
        statusCode: statusCode,
        headers: {
            'Access-Control-Allow-Origin' : '*'
        },
        body: JSON.stringify({
            success: success,
            message: msg,
        })
    };
    return response;
}