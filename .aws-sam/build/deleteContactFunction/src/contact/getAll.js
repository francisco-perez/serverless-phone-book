module.exports.getAll = async (event, dataBase) =>{

    let statusCode = 200;
    let message = '';
    let contacts = {};
    try{
        const tableName = dataBase.vars.tableName;
        const docClient = dataBase.DB();
        
        var params = {
            TableName : tableName
        };

        const data = await docClient.scan(params).promise();
        
        if( data.Items !== null && data.Items !== undefined){
            contacts = data.Items;
        }

    }catch(error){
        statusCode = 500;
        message = JSON.stringify(error);
        console.info(message);
    }

    const response = {
        statusCode: statusCode,
        headers: {
            'Access-Control-Allow-Origin' : '*'
        },
    };

    if( statusCode == 200){
        response.body =  JSON.stringify(contacts);
    }else{
        response.body = message;
    }
    return response;
}