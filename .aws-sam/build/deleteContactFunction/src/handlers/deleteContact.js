/**
 * Add a contact to phone book table
 */
exports.deleteContactHandler = async (event) => {
    if ( event.httpMethod !== null && event.httpMethod !== undefined 
        && event.httpMethod !== 'DELETE') {
        throw new Error(`This function only accepts DELETE method, you tried: ${event.httpMethod} method.`);
    }
    
    const dataBase = require('../database/DBManager');
    dataBase.vars.databaseType = process.env.DB_TYPE;
    const contact = require('../contact/delete');
    const response = contact.delete(event, dataBase);
    console.info(`response from: ${event.path} statusCode: ${response.statusCode} body: ${response.body}`);
    
    return response;
}
