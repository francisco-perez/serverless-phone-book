
/**
 * Add a contact to phone book table
 */
exports.getContactByIdHandler = async (event) => {
    if ( event.httpMethod !== null && event.httpMethod !== undefined 
        && event.httpMethod !== 'GET') {
        throw new Error(`This function only accepts GET method, you tried: ${event.httpMethod} method.`);
    }
    
    const dataBase = require('../database/DBManager');
    dataBase.vars.databaseType = process.env.DB_TYPE;
    const contact = require('../contact/getById');
    const response = contact.getById(event, dataBase);
    return response;
}
