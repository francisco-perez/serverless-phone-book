
/**
 * Get all contacts from table
 */
exports.getContactsHandler = async (event) => {
    if ( event.httpMethod !== null && event.httpMethod !== undefined 
        && event.httpMethod !== 'GET') {
        throw new Error(`This function only accepts GET method, you tried: ${event.httpMethod} method.`);
    }
    
    const dataBase = require('../database/DBManager');
    dataBase.vars.databaseType = process.env.DB_TYPE;
    const contact = require('../contact/getAll');
    const response = contact.getAll(event, dataBase);
    return response;
}
