const AWS = require('aws-sdk')
AWS.config.update({ region: process.env.AWS_REGION })
const s3 = new AWS.S3()
const URL_EXPIRATION_SECONDS = 900
const { v4: uuidv4 } = require('uuid');
const fs = require('fs')
const readline = require('readline')
const dataBase = require('../database/DBManager');
dataBase.vars.databaseType = process.env.DB_TYPE;
const docClient = dataBase.DB();    
// Main Lambda entry point
exports.getPresignedURLHandler = async (event) => {
    return await getUploadURL(event);
}

const getUploadURL = async function(event) {
    
    const randomID = parseInt(Math.random() * 10000000)
    const Key = `${randomID}.csv`

    // Get signed URL from S3
    const s3Params = {
        Bucket: process.env.UploadBucket,
        Key,
        Expires: URL_EXPIRATION_SECONDS,
        ContentType: 'text/csv',        
        ACL: 'public-read'
    }
    const uploadURL = await s3.getSignedUrlPromise('putObject', s3Params)
    console.info(uploadURL);
    const response = {
        statusCode: 200,
        headers: {
            'Access-Control-Allow-Origin': '*',
        },
        body: JSON.stringify({
            encodeURL: encodeURI(uploadURL),
            decodeURL: decodeURI(uploadURL),
            uploadURL: uploadURL,
            Key
        })
    };
    return response;
}

exports.saveCSVHandler = async (event) => {
    
    let body = JSON.parse(event.body);
    const key = body.key;
    const bucketName = process.env.UploadBucket;
    
    
    // const data = await s3.getObject({ 
    //     Bucket: bucketName,
    //     Key: key 
    // }).promise();
    //var out = fs.createWriteStream(key);
    const params = { 
        Bucket: bucketName,
        Key: key 
    }
    //const data = await s3.getObject(params).promise().createReadStream();

    const data = s3.getObject(params).createReadStream();
    // const text = data.Body.toString('utf8');
    const rl = readline.createInterface({
        input: data,
        crlfDelay: Infinity
    });
    
    //console.info("Raw text:\n" + text);
    
    /*const readStream = fs.createReadStream(key, { encoding: 'utf8' })
    const rl = readline.createInterface({
      input: readStream,
      crlfDelay: Infinity
    })*/
    //const rl = text.split('\r\n');
  
    const batchSize = 25
    const concurrentRequests = 50
    let firstLine = true
    let items = []
    let batchNo = 1
    let promises = []
    let id;
    const tableName = dataBase.vars.tableName;
    console.info(` == TABLE: ${tableName} ==`)
    console.info(' == PROCESS BEGIN == ')
    for await (const line of rl) {
        if (firstLine) {
            firstLine = false
            continue
        }
    
        id = uuidv4();
        const obj = convertToObject(line,key,id)
        if (obj) {
            items.push(obj);
        }
    
        if (items.length % batchSize === 0) {
            if( batchNo % 80 === 0)
                console.info(` batch ${batchNo}`)
    
            promises.push(saveToDynamoDB(items,tableName))

            if (promises.length % concurrentRequests === 0) {
                console.info('\nawaiting write requests to DynamoDB\n')
                await Promise.all(promises)
                promises = []
            }
    
            items = []
            batchNo++
        }
    }
  
    if (items.length > 0) {
      console.info(` batch ${batchNo}`)
      promises.push(saveToDynamoDB(items,tableName))
    }
  
    if (promises.length > 0) {
      console.info('\nawaiting write to DynamoDB\n')
      await Promise.all(promises)
    }
    
    console.info(' == PROCESS END == ')
    const response = {
        statusCode: 200,
        headers: {
            'Access-Control-Allow-Origin' : '*',
        },
        body: JSON.stringify({
            success: true,
            message: 'finish DB processing',
            id: key
        })
    };
    return response;
}

function convertToObject (line, key, id) {
    const items = line.split(',')
    
    if (items.length === 3 ){
        return {
            id: id,
            first_name: items[0],
            last_name: items[1],
            phone: items[2],
            file: key
        }
    } else return null
}
  
async function saveToDynamoDB (items, tName) {
    const putReqs = items.map(item => ({
        PutRequest: {
            Item: item
        }
    }))

    const req = {
        RequestItems: {
            [tName]: putReqs
        }
    }

    await docClient.batchWrite(req).promise()
};