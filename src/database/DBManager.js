
let DBVars = {
    tableName: process.env.PHONE_BOOK,
    databaseType: ''
};

module.exports.DB = () => {
    let db_select;
    switch(DBVars.databaseType){
        case 'dynamo': 
            const dataBaseDyn = require('./dynamo');
            db_select = dataBaseDyn.getClient();
            break;
    }
    return db_select;
};

module.exports.vars = DBVars;
