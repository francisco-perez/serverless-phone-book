'use strict';

module.exports.createUpdate = async (event, dataBase, id = null) => {
    
    let statusCode = 200;
    let success = false;
    let msg = '';
    let v_id='';

    try{
        const tableName = dataBase.vars.tableName;
        console.info(tableName);
        const docClient = dataBase.DB();

        let action = 'created';
        if( event.pathParameters !== null && event.pathParameters !== undefined
            && event.pathParameters.id !== null && event.pathParameters.id !== undefined){
            action = 'updated';
        }

        // Get info from the body of the request
        let body;
        if( event.body !== null && event.body !== undefined){
            body = JSON.parse(event.body);
            if( body.body !== null && body.body !== undefined){
                body = JSON.parse(body.body);
            }
        }else{
            msg = 'No information received in body request, please send Contact information in body request.';
            throw new Error(msg);
        }

        let v_first_name = '';
        let v_last_name = '';
        let v_phone = '';

        if( body !== null && body !== undefined ){

            v_first_name = body.first_name!==undefined?body.first_name:'';
            v_last_name = body.last_name!=undefined?body.last_name:'';
            v_phone = body.phone!==undefined?body.phone:'';
            
            // generate id for a new contact, or get the id from the event parameter
            if( action == 'updated'){
                v_id = event.pathParameters.id;
            }else{
                v_id = id;
            }
        }

        console.info('Action: ' + action);
        // if update a contact, you need to update at least 1 field
        if( action == 'updated' && (v_first_name.length == 0 && v_last_name.length== 0 && v_phone.length == 0) ){
            msg = 'To update a contact, please declare one of the following: first name, last name or phone';
        }

        // if create a contact, you need to declare 3 fields
        if( action == 'created' && (v_first_name.length == 0 || v_last_name.length==0 || v_phone.length == 0) ){
            msg = 'Missing information: please declare first name, last name and phone';
        }

        if( msg.length > 0 ){
            statusCode = 400;
        }else{
            
            console.info('Saving to DB');
            var dbItem = { 
                first_name: v_first_name,
                last_name: v_last_name,
                phone: v_phone,
            };
        
            let params = {};
            params.TableName = tableName;
            let result;
            if( action == 'created' ){
                params = {
                    ...params,
                    ...getCreateParams(dbItem, v_id)
                };
                result = await docClient.put(params).promise();
            }else{
                params = {
                    ...params,
                    ...getUpdateParams(getItemValues(dbItem), v_id)
                };
                result = await docClient.update(params).promise();
            }

            success = true;
            msg = 'Contact ' + action + ' successfully!';
            console.info('DB passed: ' + msg);
        }

    }catch(error){
        success = false;
        console.info(error);
        if(error !== undefined && error.message !== undefined){
            msg = error.message;
            if( error.message == 'The conditional request failed' || error.code == 'ConditionalCheckFailedException'){
                statusCode = 404;
                msg = "Contact not found, check id";
            }
        }else{
            msg = 'Error trying to create a new contact.';
        }
        
            
    }

    const response = {
        statusCode: statusCode,
        headers: {
            'Access-Control-Allow-Origin' : '*',
            'Access-Control-Allow-Methods': 'POST, PUT, GET, OPTIONS',
            'Access-Control-Allow-Headers': 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With'
        },
        body: JSON.stringify({
            success: success,
            message: msg,
            id: success?v_id:''
        })
    };

    return response;
}

function getItemValues(dbItem){
    var newItem = {};
    if( dbItem.first_name.length > 0 ){
        newItem.first_name = dbItem.first_name;
    }

    if( dbItem.last_name.length > 0 ){
        newItem.last_name = dbItem.last_name;
    }

    if( dbItem.phone.length > 0 ){
        newItem.phone = dbItem.phone;
    }

    return newItem;
}

function getCreateParams(dbItem, id){
    dbItem.id = id;
    var params = {
        Item : dbItem
    };
    return params;
}

function getUpdateParams(dbItem, id){
    var params = {
        Key: {
            "id": id
        }
    };
    params.UpdateExpression = 'set ' + Object.keys(dbItem).map(k => `#${k} = :${k}`).join(', ');
    params.ConditionExpression = 'id = :id';
    params.ExpressionAttributeNames = Object.entries(dbItem).reduce((acc, cur) => ({...acc, [`#${cur[0]}`]: cur[0]}), {});
    params.ExpressionAttributeValues = {
        ...Object.entries(dbItem).reduce((acc, cur) => ({...acc, [`:${cur[0]}`]: cur[1]}), {}),
        ...{':id':id}
    };
    params.ReturnValues = "ALL_NEW";
    return params;
}