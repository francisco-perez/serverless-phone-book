module.exports.getAll = async (event, dataBase) =>{

    let statusCode = 200;
    let message = '';
    let contacts = {};
    let total_count = '';
    let page_count = '';
    let last_eval_key = '';
    try{
        const tableName = dataBase.vars.tableName;
        const docClient = dataBase.DB();

        let params = {};
        //console.info(event);
        //console.info(event.queryStringParameters);
        const body = event.queryStringParameters;
        if( event.queryStringParameters !== null){
            
            
            //params.ConsistentRead = false;
            if( body.filename !== null && body.filename.length > 0){
                params.ConsistentRead = false;
                params.FilterExpression = "#971c0 = :971c0",
                params.ExpressionAttributeValues = {
                    ":971c0": body.filename
                },
                params.ExpressionAttributeNames = {
                    "#971c0": "file"
                }
            }

            if( body.last_eval_key !== null 
                && body.last_eval_key !== undefined
                && body.last_eval_key.length > 0)
            {
                params.ExclusiveStartKey = JSON.parse(body.last_eval_key);
            }
        }
        params.TableName = tableName;
        params.Limit = body.limit!==null && body.limit!==undefined?body.limit:10;

        console.info("Params LOG: ");
        console.info(params);
        const data = await docClient.scan(params).promise();
        //console.info(data);
        if( data.Items !== null && data.Items !== undefined){
            contacts = data.Items;
            total_count = data.Count;
            page_count = data.ScannedCount;
            last_eval_key = data.LastEvaluatedKey;
        }

    }catch(error){
        console.info(error);
        statusCode = 500;
        message = JSON.stringify(error);
        if( message === null || message === undefined || message.length == 0){
            message = "Internal server error";
        }
    }

    const response = {
        statusCode: statusCode,
        headers: {
            'Access-Control-Allow-Origin' : '*',
            'Access-Control-Allow-Methods': 'POST, PUT, GET, OPTIONS',
            'Access-Control-Allow-Headers': 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With'
        },
    };

    if( statusCode == 200){
        response.body =  JSON.stringify({
            'contact_items': contacts,
            'total_count': total_count,
            'page_count': page_count,
            'last_eval_key': last_eval_key
        });
    }else{
        response.body = message;
    }
    return response;
}